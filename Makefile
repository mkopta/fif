# fif - fif is fifteen
# See LICENSE file for copyright and license details.

include config.mk

SRC = fif.c
OBJ = ${SRC:.c=.o}

all: options fif

options:
	@echo fif build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: config.mk

fif: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ fif.o ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f fif ${OBJ} fif-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p fif-${VERSION}
	@cp -R LICENSE Makefile config.mk README \
		fif.1 ${SRC} fif-${VERSION}
	@tar -cf fif-${VERSION}.tar fif-${VERSION}
	@gzip fif-${VERSION}.tar
	@rm -rf fif-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f fif ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/fif
	@echo installing manual page to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed "s/VERSION/${VERSION}/g" < fif.1 > ${DESTDIR}${MANPREFIX}/man1/fif.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/fif.1

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/fif
	@echo removing manual page from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/fif.1

.PHONY: all options clean dist install uninstall
