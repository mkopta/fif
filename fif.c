#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include <assert.h>

#define SHUFFLE_LEVEL 1000

enum action_type {
	LEFT, RIGHT, UP, DOWN
};

struct gamestate {
	int *board;
	char empty_piece_location[2];
};

void print_horizontal_line(int size)
{
	int i;
	for (i = 0; i <= size; i++) {
		printw("+");
		if (i != size)
			printw("-----");
	}
	printw("\n");
}

void print_board(const struct gamestate state, int size)
{
	int x, y, num;
	clear();
	print_horizontal_line(size);
	for (x = 0; x < size; x++) {
		printw("|");
		for (y = 0; y < size; y++) {
			num = state.board[size * x + y];
			if (num) printw(" %3d |", num);
			else printw("     |");
		}
		printw("\n");
		print_horizontal_line(size);
	}
}

struct gamestate create_base_board(int size)
{
	int x, y;
	int *board = (int *) malloc(sizeof(int) * size * size);
	if (!board) {
		perror("malloc");
		exit(1);
	}
	struct gamestate gs;
	gs.board = board;
	for (x = 0; x < size; x++)
		for (y = 0; y < size; y++)
			gs.board[size * x + y] =
				(size * x + y + 1) % (size * size);
	gs.empty_piece_location[0] = size - 1;
	gs.empty_piece_location[1] = size - 1;
	return gs;
}

void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int action(enum action_type a, struct gamestate *gs, int size)
{
	int x = gs->empty_piece_location[0],
	    y = gs->empty_piece_location[1];
	switch (a) {
	case LEFT:
		if (y == size - 1)
			return 0;
		swap(&(gs->board[size * x + y]),
			&(gs->board[size * x + (y + 1)]));
		y++;
		break;
	case RIGHT:
		if (y == 0)
			return 0;
		swap(&(gs->board[size * x + y]),
		&(gs->board[size * x + (y - 1)]));
		y--;
		break;
	case UP:
		if (x == size - 1)
			return 0;
		swap(&(gs->board[size * x + y]),
		&(gs->board[size * (x + 1) + y]));
		x++;
		break;
	case DOWN:
		if (x == 0)
			return 0;
		swap(&(gs->board[size * x + y]),
			&(gs->board[size * (x - 1) + y]));
		x--;
		break;
	default:
		fprintf(stderr, "fif: Internal error.\n");
		exit(1);
	}
	gs->empty_piece_location[0] = x;
	gs->empty_piece_location[1] = y;
	return 1;
}

void shuffle(struct gamestate *gs, int size)
{
	int i;
	for (i = 0; i < SHUFFLE_LEVEL * size * size; i++)
		(void) action(rand() % 4, gs, size);
}

void print_help(void)
{
	clear();
	printw("fif - fif is fifteen\n");
	printw("Martin Kopta <martin@kopta.eu>\n");
	printw("Licensed under ISC license\n");
	printw("\nCommands:\n");
	printw("  up      k\n");
	printw("  down    j\n");
	printw("  left    h\n");
	printw("  right   l\n");
	printw("  help    ?\n");
	printw("  quit    q\n");
	(void) getch();
}

int count_number_of_tiles_off_place(struct gamestate gs, int size)
{
	int x, y, count = 0;
	for (x = 0; x < size; x++)
		for (y = 0; y < size; y++)
			if (gs.board[size * x + y] !=
				(size * x + y + 1) % (size * size))
				count++;
	return count;
}

int game_over(struct gamestate gs, int size)
{
	return count_number_of_tiles_off_place(gs, size) == 0;
}

int play(struct gamestate gs, int size)
{
	int c, moves = 0;
	enum action_type at = 0;
loop:
	print_board(gs, size);
	if (game_over(gs, size)) return moves;
	refresh();
	c = getch();
	if (c == 'q') {
		endwin();
		exit(0);
	} else if (c == 'h' || c == 'j' || c == 'k' || c == 'l') {
		moves++;
		if (c == 'h') at = LEFT;
		if (c == 'j') at = DOWN;
		if (c == 'k') at = UP;
		if (c == 'l') at = RIGHT;
		action(at, &gs, size);
	} else print_help();
	goto loop;
	return moves;
}

int main(int argc, char **argv)
{
	int size = 4, moves_count = 0;
	struct gamestate gs;
	if (argc == 2)
		size = atoi(*++argv);
	if (size < 2 || size > 20) {
		fprintf(stderr, "Minimum size = 2, maximum = 20\n");
		exit(1);
	}
	srand((unsigned) time(NULL));
	initscr();
	curs_set(0);
	noecho();
	gs = create_base_board(size);
	shuffle(&gs, size);
	moves_count = play(gs, size);
	free(gs.board);
	printw("You win. Moves: %d\n", moves_count);
	refresh();
	(void) getch();
	endwin();
	return 0;
}
