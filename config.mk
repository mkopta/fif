# fif version
VERSION = 2316

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# includes and libs
INCS = -I/usr/include
LIBS = -lncurses

# flags
CFLAGS = -std=c99 -pedantic -Wall -Os ${INCS}
LDFLAGS = ${LIBS}

# compiler and linker
CC = cc
